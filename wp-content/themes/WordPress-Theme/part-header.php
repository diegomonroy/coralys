<!-- Begin Top -->
	<section class="top_1 wow bounceInDown" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'top_1' ); ?>
			</div>
			<div class="show-for-small-only">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
		</div>
	</section>
	<section class="top wow bounceInDown" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 columns">
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->